//
//  BattleScene.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 19/12/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit
import SpriteKit

// SKPhysicsContactDelegate : Implementar metodos de contacto en los objetos de la pantalla
// SKScene debe ir inscrustada en un viewController, Una Scene necesita un viewController

class BattleScene : SKScene {
    
    var pokemon : Pokemon!
    
    // NODOS A CREAR DENTRO DE LA ESCENA
    var pokemonSprite : SKNode!
    var pokeballSprite : SKNode!
    
    // VARIABLES DEL JUEGO QUE NO CAMBIAN "kNombreVariable"
    let kPokemonSize : CGSize = CGSize(width: 80, height: 80) //Tamaño del pokemon
    let kPokemonName : String = "pokemon"
    let kPokeballName : String = "pokeball"
    
    // VARIABLES DE MOVIMIENTO, PARAMETRIZAR SKAction
    let pokemonDistance  = 150.0
    let pokemonPixelPerSecond = 80.0
    
    // VARIABLES INTERACTUACION, CATEGORIAS DE CHOQUE : SE PUEDE COLICIONAR CON 3 TIPOS DE OBJETOS POKEMON, POKEBALL Y EL BORDE DE LA PANTALLA
    // Para checar coliciones se realiza a nivel binario con AND y OR
    let kPokemonCategory : UInt32 = 0x1 << 0  // bit numero 0
    let kPokeballCategory : UInt32 = 0x1 << 1  // bit numero 1
    let kSceneCategory : UInt32 = 0x1 << 2  // bit numero 2
    
    
    // VARIABLES DE LANZAMIENTO DE LA POKEBALL
    var velocity : CGPoint = CGPoint.zero
    var touchPoint : CGPoint = CGPoint()  // Donde toca la pantalla el usuario
    var canThrowPokeball = false //Tirar la pokeball : Tocar la pokeball y arrastrar adecuadamente por la pantalla
    
    // VARIABLE CONTACTO ENTRE POKEMON Y POKEBALL
    var pokemonCaught = false
    var startCount = true  // empieza conteo tiempo de captura
    var maxTime = 15  // Tiempo de Captura
    var myTime = 30
    var printTimeLabel = SKLabelNode(fontNamed: "Arial")  // Label de SKScene
    
    
    
    
    // Metodo arranca una SKScene en spritkit : Similar a viewDidLoad()
    override func didMove(to view: SKView) {
        
        // CREAR IMAGEN FONDO DE LA BATALLA
        let bgImage = SKSpriteNode(imageNamed: "background")  // Creacion nodo a partir de un nombre de imagen
        bgImage.size = self.size  // tamaño de la imagen al tamaño de la escena
        bgImage.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2) // posicion de la imagen centro de la escena
        bgImage.anchorPoint = CGPoint(x: 0.5, y: 0.5) // ancorPoint por defecto
        bgImage.zPosition = -1 // Enviamos al fondo de la escena
        self.addChild(bgImage)  //agregamos el sprite a la escena
        
        
        // POSICION DEL LABEL DEL TIEMPO DE LA BATALLA EN LA ESCENA
        self.printTimeLabel.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.9)  // self.size.height * 0.9 : 90% de la pantalla
        self.addChild(self.printTimeLabel)
        
        // AÑADIR EL POKEMON DESPUES DE LA SKSCENE
        self.perform(#selector(setupPokemon), with: nil, afterDelay: 2.0)
        self.perform(#selector(setupPokeball), with: nil, afterDelay: 2.0)
        
        
        // INDICAR EL USO DE FISICA EN EL JUEGO
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame) // Indicamos el limite de el juego sera el borde de nuestra pantalla
        self.physicsBody!.categoryBitMask = kSceneCategory // Categoria de la fisica borde de la pantalla
        self.physicsWorld.contactDelegate = self  // Indicamos que nos encargaremos de las coliciones de dos cuerpos fisicos
        
        // SPRITE MENSAJE DE INICIO DE LA BATALLA
        showMessageWith(imageNamed: "battle")
        
       
    }
    
    // CREACION DEL SPRITE
    func createPokemon() -> SKNode {
        
        //Creacion de un SKNode de tipo Sprite
        let pokemonSprite = SKSpriteNode(imageNamed: self.pokemon.imageName!)  // Creamos un spriteNode con nombre de imagen
        pokemonSprite.size = kPokemonSize  // Configuramos el tamaño del Sprite
        pokemonSprite.name = kPokemonName  // Nombramos al Sprite
        
        return pokemonSprite
    }
    
    // CONFIGURACION DEL POKEMON
    func setupPokemon() {
        self.pokemonSprite = createPokemon()  //ejecutamos la accion de creacion del spriteNode
        self.pokemonSprite.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)  // posicion del sprite en medio del padre SKScene
        
        self.pokemonSprite.physicsBody = SKPhysicsBody(rectangleOf: self.kPokemonSize) // Definimos un region de colicion de fisica para el sprite Pokemon
        
        // INDICAR EL POKEMON Y LA POKEBALL ESTARAN SOMETIDAS A FISICA
        self.pokemonSprite.physicsBody!.isDynamic = false  //movimiento constante choques e interactuacion
        self.pokemonSprite.physicsBody!.affectedByGravity = false   //Afectado por la gravedad
        self.pokemonSprite.physicsBody!.mass = 1.0  //Cuanto debe pesar el pokemon, podemos guardar la masa en la entidad y que sea distinta segun el elemento pokemon
        
        self.pokemonSprite.physicsBody!.categoryBitMask = kPokemonCategory  //Categoria y contra quien debe colicionar
        self.pokemonSprite.physicsBody!.contactTestBitMask = kPokeballCategory  // con quien puede interactuar
        self.pokemonSprite.physicsBody!.collisionBitMask = kSceneCategory //categorias colicionar por efecto fisico : chocar por medio de la fisica es el borde
        
        
        // CREACION SECUENCIA DE ACCIONES MOVIMIENTO CENTRO -> DERECHA, DERECHA -> CENTRO, CENTRO -> IZQUIERDA, IZQUIERDA -> CENTRO
        let moveRigth = SKAction.moveBy(x: CGFloat(self.pokemonDistance), y: 0, duration: self.pokemonDistance / self.pokemonPixelPerSecond)
        let sequence = SKAction.sequence([moveRigth, moveRigth.reversed(), moveRigth.reversed(), moveRigth]) // Secuencia de animacion horizontal
        // self.pokemonSprite.run(sequence)  //Ejecutaria una vez
        self.pokemonSprite.run(SKAction.repeatForever(sequence))  // Repite infinitamente
        
        
        self.addChild(self.pokemonSprite)  //Agregamos el sprite a la SKScene de la batalla
        
    }
    
    // CREACION DE LA POKEBALL
    func createPokeball() -> SKNode {
        
        let pokeballSprite = SKSpriteNode(imageNamed: self.kPokeballName)
        pokeballSprite.size = kPokemonSize
        pokeballSprite.name = kPokeballName
    
        return pokeballSprite
    }
    
    // CONFIGURACION DE POKEBALL CON RESPECTO A LA ESCENA
    func setupPokeball() {
        self.pokeballSprite = createPokeball()
        self.pokeballSprite.position = CGPoint(x: self.size.width / 2, y: 50)
        
        
        self.pokeballSprite.physicsBody = SKPhysicsBody(circleOfRadius: self.pokeballSprite.frame.size.width / 2.0)  // Creacion region fisica de colicion
        self.pokeballSprite.physicsBody!.isDynamic = true  // Necesita rebotar
        self.pokeballSprite.physicsBody!.affectedByGravity = true //Tiene que llegar abajo de la pantalla
        self.pokeballSprite.physicsBody!.mass = 1.0  // peso
        
        self.pokeballSprite.physicsBody!.categoryBitMask = kPokeballCategory
        self.pokeballSprite.physicsBody!.contactTestBitMask = kPokemonCategory  // Checar el contacto
        self.pokeballSprite.physicsBody!.collisionBitMask = kSceneCategory | kPokemonCategory //Colisiona contra el borde de la escenea y contra el pokemons
        
        
        self.addChild(self.pokeballSprite)
        
    }
    
    
    // INDICA SI SE PUEDE LANZAR LA POKEBALL, CUMPLE CON LAS RESTRICCIONES
    // Cuando comienza el toque en la ventana contenedor : Toco la pokeball
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first  // Ultimo toque en la pantalla
        if let location = touch?.location(in: self){ // Localizacion de ese toque dentro de la pantalla, location : CGPoint
          
            // Preguntar si en esa localizacion esta la pokeball
            if self.pokeballSprite.frame.contains(location){
                self.canThrowPokeball = true
                self.touchPoint = location  // guardamos la localizacion del toque hecha a la pokeball
            }
        }
        
    }
    
    
    // Cuando el dedo es levantado de la ventana : Fuerza de lanzamiento
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        if let location = touch?.location(in: self)  {// Localizacion del toque en la pantalla
            self.touchPoint = location
            
            if canThrowPokeball{ // Se puede lanzar pokeball
                throwPokeball()
            }
        }
        
    }
    
   
    func throwPokeball() {
        self.canThrowPokeball = false
        
        // DAR IMPULSO, VELOCIDAD A POKEBALL
        let dt : CGFloat = 1.0 / 120.0  // diferencial de tiempo, fuerza de lanzamiento de la pokeball
        
        let distance = CGVector(dx: self.touchPoint.x - self.pokeballSprite.position.x , dy: self.touchPoint.y - self.pokeballSprite.position.y) // direccion y distancia lanzar la pokeball : pokeball.position - touchended.location
        
        let velocity = CGVector(dx: distance.dx / dt, dy: distance.dy / dt)  // Velocidad de lanzamiento con respecto al la fuerza y distacia del toque
        
        self.pokeballSprite.physicsBody!.velocity = velocity  // Asignamos velocidad a la pokeball
        
    }
    
    
    
    // ACTUALIZA LO QUE HAY EN PANTALLA
    // Se ejecuta 60 veces por segundo : 60 fps
    override func update(_ currentTime: TimeInterval) {  // currentTime : Time in seconds
        
        // TEMPORIZADOR  DISMINUCION DEL TIEMPO DE LA BATALLA
        if self.startCount{
            
            // CUENTA ATRAS USANDO EL TIEMPO ACTUAL EN SEGUNDOS
            self.maxTime = Int(currentTime) + self.maxTime  // 8 + 30 = 38
            self.startCount = false
            
        }
        
        self.myTime = self.maxTime - Int(currentTime) // 38 - 8 = 30
        self.printTimeLabel.text = "\(self.myTime)"
        
        if self.myTime <= 0 {   // Termina el juego al llegar a o el tiempo de la batalla
            endGame()
        }
        
        
    }
    
  
    
    
    
}

extension BattleScene : SKPhysicsContactDelegate{
    
    
    // SABER SI DOS ELEMENTOS HAN CONTACTADO EN PANTALLA
    func didBegin(_ contact: SKPhysicsContact) {  // called when two bodies contact each other
        
        // COMPROBAR EN TIPO DE CONTACTO
        let contactMaks = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask  // suma de bits en  OR
        
        
        // Verificar si contiene la categoria pokemon o pokeball
        switch contactMaks {
        case kPokemonCategory | kPokeballCategory:
            print("Contacto entre el Pokemon y la Pokeball")
            self.pokemonCaught = true
            endGame()
            
        default:
            return
        }
    }
    
    
    func endGame(){
        
        // QUITAMOS A LOS SPRITE DE LA PANTALLA
        self.pokemonSprite.removeFromParent()
        self.pokeballSprite.removeFromParent()
        
        if self.pokemonCaught {
            print("Pokemon Capturado")
            
            // PERSISTIR LA CAPTURA DEL POKEMON
            if let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer{
                
                let context = container.viewContext
                
                self.pokemon.timesCaught += 1
                
                do {
                    try context.save()
                } catch let error {
                    print("Error al persistir Captura pokemon", error.localizedDescription)
                }
            }

            
            showMessageWith(imageNamed: "gotcha")
            
            
            
        }else{
            print("Me he quedo sin tiempo")
            showMessageWith(imageNamed: "footprints")
            
        }
        
        self.perform(#selector(endBattle), with: nil, afterDelay: 1.0)  // Al final de la batalla desaparecen los mensajes de gotcha o footprints
        
    }
    
    
    // CREACION DE UN  MENSAJE SPRITENODE
    func showMessageWith(imageNamed : String){
        let message = SKSpriteNode(imageNamed: imageNamed)
        message.size = CGSize(width: 150, height: 150)
        message.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        self.addChild(message)
        
        
        // ESPERA UN SEGUNDO EN PANTALLA Y DESAPARECE DESPUES DE ESE SEGUNDO
        message.run(SKAction.sequence([SKAction.wait(forDuration: 2.0), SKAction.removeFromParent()]))
    
    }
    
    // LANZARA UNA NOTIFICACION RECIBE BattleSceneViewController FIN DE LA BATALLA
    func endBattle(){
        NotificationCenter.default.post(name: NSNotification.Name("closeBattle"), object: nil)  // Enviar una notificacion dada al Receptor : EL OBSERVADOR
    }

}
