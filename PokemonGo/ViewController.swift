//
//  ViewController.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 20/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet var mapView: MKMapView!
    var updateCount : Int = 0  //numero de veces localiza el GPs la posicion del usuario
    var manager = CLLocationManager() //Encargado de la posicion y perisos ubicar al usuario
    let mapDistance : CLLocationDistance = 350
    let captureDistance : CLLocationDistance = 150
    let pokemonSpawnTimer : TimeInterval = 10 //lapso aparecera un objeto(Pokemon)
    var hasStartedTheMap = false  // BANDERA EJECUTA FUNC setupMap()
    
    
    var pokemons : [Pokemon] = [] //contendra los objetos de tipo Pokemons
    
    // VARIABLES DE APARICION DEL POKEMON
    var totalFrequency = 0
    
    // VARIABLES PAUSAR GENERACION DE POKEMONES CUANDO SALIMOS DEL MAPA
    var timer : Timer!
    var hasMovedAnotherView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pokemons = getAllThePokemons()
        // createAndCaughtPokemon(name: "Weedle", with: "weedle") CUA TEST
        
        
        // IMPRIMIR LA FRECUANCIA DE LOS POKEMONES
        for p in self.pokemons {
            self.totalFrequency += Int(p.frequency)  // FRECUENCIA TOTAL CONVERTIR INT16 A INT CASTING
            //print("Nombre del pokemon", p.name!, " - ", p.frequency)
        }

        print("Total frequency : ", self.totalFrequency)

        
        
        // DETERMINAR EL PESO TOTAL DE LOS POKEMONES EN FRECUENCIA : CALCULAR LA FRECUANCIA DEL POKEMON PORCENTAJE
        
        
        //PETICION DE AUTORIZACION  LOCALIZACION DEL USUARIO
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
         
            setupMap()
        }else{
            self.manager.requestWhenInUseAuthorization() //Pedir posicion solo cuando la app esta en uso
        }
        
        self.manager.delegate = self //ViewController respondera alas acciones de CLLocationManager
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Core Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) { //Se ejecuta despues de actualizar la posicion del usuario
        
        if updateCount < 4{  //intentos del gps encontrar al usuario y crear la region
            if let coordinate = self.manager.location?.coordinate{
                let region = MKCoordinateRegionMakeWithDistance(coordinate, mapDistance, mapDistance) //Parametros construir la region Coordenas y zoom 1000 metros
                self.mapView.setRegion(region, animated: true) //Generamos la region en el mapView
                updateCount += 1
            }else{
                self.manager.stopUpdatingLocation() //Si no encuentra la posicion evitamos llamar a este metodo didUpdateLocations() que se ejecuta cada segundo
            }
        }
    }
    
    
    // METODO SE EJECUTA CAMBIAN LOS PERIMISO DE LOCALIZACION
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            setupMap()
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.timer.invalidate()  // Invalidar un timer
        self.hasMovedAnotherView = true  // Se ha movido a otra vista pero no es la primera vez
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.hasMovedAnotherView{
            self.startTimer()
        }
        
    }
    
    // INICIA EL TIMER DE GENERACION DE POKEMONES EN EL MAPA
    func startTimer(){
        
        self.timer = Timer.scheduledTimer(withTimeInterval: pokemonSpawnTimer, repeats: true, block: { (Timer) in //metodo que se ejecuta un determinado tiempo acciones repetidas
            //print("Aparicion de un pockemon cada \(self.pokemonSpawnTimer)")
            
            if let coordinate = self.manager.location?.coordinate{ //Recuperamos la coordenada del usuario donde se colocara la marca
                
                //CAMBIANDO LA FRECUANCIA DE APARICION
                var pokemon : Pokemon = self.pokemons[0]
                let randomNumber = Int(arc4random_uniform(UInt32(self.totalFrequency)))  // numero aleatorio 0 y totalFrecuency
                var pokemonFrequenciesAcum = 0  // Cuando se llegue a un pokemon su frecuencia
                
                // RECORRER LOS POKEMON OCURRA
                for p in self.pokemons{
                    
                    // MIENTRAS randomNumber < que las frecuancias acumuladas
                    pokemon = p
                    pokemonFrequenciesAcum += Int(p.frequency) // frecuancia de los pokemones recorridos
                    
                    if pokemonFrequenciesAcum >= randomNumber{
                        break
                    }
                    
                }
                
                
                // CREACION DE UN POKEMON AL AZAR
                //let randomPos = Int(arc4random_uniform(UInt32(self.pokemons.count)))  // generar numero aleatoria de 0 a 27
                //let pokemon = self.pokemons[randomPos]  // del numero aleatorio generado indicamos la posicion a pasar a la annotation
                
                // USAREMOS LA SUBCLASE CREADA LLAMADA PokemonAnnotation
                let annotation = PokemonAnnotation(coordinate: coordinate, pokemon: pokemon) //Creamos la chincheta de la subclase creada
                annotation.coordinate.latitude += (Double(arc4random_uniform(1000)) - 500.0) / 400000.0
                annotation.coordinate.longitude += (Double(arc4random_uniform(1000)) - 500.0) / 400000.0
                
                self.mapView.addAnnotation(annotation) //Colocamos la marca en el mapa
            }
        })
    
    }
    
    func setupMap(){
        
        if !hasStartedTheMap{
            
            self.hasStartedTheMap = true
            
            self.mapView.showsScale = true
            self.mapView.showsBuildings = true
            self.mapView.showsUserLocation = true //mostrar la posicion del usuario
            self.manager.startUpdatingLocation()  //empezara a ubicar la localizacion del usuario se ejecuta repetidamente
            
            self.mapView.delegate = self  // lo usaremos para configurar la imagen de la chincheta
            
            self.startTimer()
            
            
        }
        
    }
    
    
    @IBAction func updateUserLocaton(_ sender: UIButton) {
        
        if let coordinate = self.manager.location?.coordinate{
            let region = MKCoordinateRegionMakeWithDistance(coordinate, mapDistance, mapDistance) //Parametros construir la region Coordenas y zoom 1000 metros
            self.mapView.setRegion(region, animated: true) //Generamos la region en el mapView
        }
    }
    
    //hidden status Bar
    override var prefersStatusBarHidden: Bool{
        return true
    }

}

// MARK: - MKMapViewDelegate
extension ViewController : MKMapViewDelegate{
    
    // USAMOS CONFIGURAR LA CHINCHETA INFORMACION E IMAGEN A MOSTAR (MODAL, INFORMACION)
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
        
        if annotation is MKUserLocation{   // Si es la chincheta de la localizacion del usuario colocar la imagen
            annotationView.image = #imageLiteral(resourceName: "player")
        }else{   // Si no es la localizacion del usuario
            
            // ACCEDEMOS A EL POKEMON ASIGNADO AL GENERAR LA ANNOTATION
            let pokemon = (annotation as! PokemonAnnotation).pokemon
            annotationView.image = UIImage(named: pokemon.imageName!) // mostramos en el mapa
            
        }
        
        // CAMBIAR EL TAMAÑO DEL FRAME DE LA IMAGEN : Frame rectangulo que contiene el tamaño, y la posicion
        var newFrame = annotationView.frame
        
        // COMO LAS IMAGENES SON CUADRADAS COLOCAMOS MISMO TAMAÑO, SI NO SON CUADRADAS REGLA DE 3 escala en altura cuanto debo escalar en ancho
        newFrame.size.height = 50
        newFrame.size.width = 50
        annotationView.frame = newFrame
        
        
        return annotationView
        
    }
    
    // FUNCION SELECCION UNA CHNCHETA EN EL MAPA
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if view.annotation is MKUserLocation{ // No peermitimos seleccionar la localizacion del usuario
            return
        }
        
        mapView.deselectAnnotation(view.annotation, animated: true) // Quitamos la seleccion a la chincheta
        
        
        // CENTRAR EL MAPA AL POKEMON SELECCIONADO
        
        if let coordinatePokemon = view.annotation?.coordinate{
            let region = MKCoordinateRegionMakeWithDistance(coordinatePokemon, captureDistance, captureDistance)
            self.mapView.setRegion(region, animated: false)
        }
        
        // COMPROBAR SI EL USUARIO SE ENCUENTRA EN EL RANGO DE VISION DEL MAPA AL MOMENTO DE CENTAR EL POKEMON
        if let coordinate = self.manager.location?.coordinate{
            
            // INDICAMOS ZONA VISIBLE DEL MAPA CON RESPECTO LAS COORDENADAS DEL USUARIO
            if MKMapRectContainsPoint(mapView.visibleMapRect, MKMapPointForCoordinate(coordinate)){   // indicamos si en el rectangulo visible del mapa se encuentra un punto (en este caso el usuario)
                
                // PRESENTAR ESCENE BattleViewController Y ENVIAR EL POKEMON A CAPTURAR
                let vc = BatttleSceneViewController()  // instancia del viewController que gobiarna a la escene de la batalla
                vc.pokemon = (view.annotation as! PokemonAnnotation).pokemon  // Accedemos a la subclase pokemonAnnotation su atributo pokemon
                
                // ELIMINAR LA ANNOTATION DEL MAPA
                self.mapView.removeAnnotation(view.annotation!)  // Puede ir en el completion del present de la battalla
                
                self.present(vc, animated: true, completion: nil) // Presentamos el viewController que tiene inscrustada la BattleScene
                
            }else{
                print("Muy Lejos para capturar al pokemon")
                
                let pokemon = (view.annotation as! PokemonAnnotation).pokemon  // Accedemos al nombre del pokemon Seleccionado
                
                let alertController = UIAlertController(title: "Estás demasiado lejos",
                                                        message: "Acercate al \(pokemon.name!) para poder capturarlo",
                                                        preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                
                
            }
        }
        
        // print("Seleccione un Pokemon")
    }
    
    
}

