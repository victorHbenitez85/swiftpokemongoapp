//
//  PokemonAnnotation.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 18/12/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

// SUBCLASE  PODEMOS USAR LA DADA POR DEFECTO Y ESTA SUBCALSE COMO HECEMOS CON UITABLEVIEWCELL

import UIKit
import MapKit

class PokemonAnnotation: NSObject, MKAnnotation {
    
    // Center latitude and longitude of the annotation view.
    var coordinate: CLLocationCoordinate2D
    var pokemon : Pokemon  // Objeto de tipo pokemon
    
    
    init(coordinate : CLLocationCoordinate2D, pokemon : Pokemon) {
        self.coordinate = coordinate
        self.pokemon = pokemon
    }

    
}
