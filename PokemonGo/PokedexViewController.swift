//
//  PokedexViewController.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 23/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var caughtPokemons : [Pokemon] = []  //Capturados
    var uncoughtPokemons : [Pokemon] = [] //No capturados

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Asignar delegados de UITableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        self.caughtPokemons = getAllCaugthPokemons()  //Toma de Core Data Helper los objetos con la peticion de timesCaught > 0
        self.uncoughtPokemons = getAllUncoughtPokemons() // get de Core data Helper los objetos de al entidad Pokemon con el atributo timesCaught == 0
        
        print("CAPTURADOS", self.caughtPokemons.count)
        print("NO CAPTURADOS", self.uncoughtPokemons.count)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backToMapPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil) //quitar la vista pokedex Cambio de ViewControllers
        
    }
    
    //hidden status Bar
    override var prefersStatusBarHidden: Bool{
        return true
    }


}


// MARK: - UITableViewDelegate

extension PokedexViewController : UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0{
            return "Capturado"
        }else{
            return "No Capturados"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return self.caughtPokemons.count  // Array de capturados
        }else{
            return self.uncoughtPokemons.count // Array de no capturados
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cellidentifier", for: indexPath) as! PokemonTableViewCell  // Uso de la subclase UITableViewCell
        
        let pokemon : Pokemon  // tomara el valor de los objetos capturados o no capturados dependiendo de la seccion.
    
        // DEPENDIENDO LA SECCION MOSTRAMOS LAS FILAS DE CAPTURADOS O NO CAPTURADOS
        if indexPath.section == 0{  // SECCION POKEMONS CAPTURADOS
            pokemon = self.caughtPokemons[indexPath.row]
            cell.pokemonTimesCaughtLabel.text = "Capturado : \(pokemon.timesCaught)"
            
        }else{ //SECCION POKEMONS NO CAPTURADO
            
            pokemon = self.uncoughtPokemons[indexPath.row]
            cell.pokemonTimesCaughtLabel.text = ""
        }
        
        
        cell.pokemonImageView.image = UIImage(named: pokemon.imageName!)  // Uso de la celda personalizada
        cell.pokemonTitleLabel.text = pokemon.name
        //cell.pokemonTimesCaughtLabel.text = "\(pokemon.timesCaught)"
        
        return cell
        
    }

}


// MARK: - UITableViewDataSource
extension PokedexViewController : UITableViewDataSource{

    
}

