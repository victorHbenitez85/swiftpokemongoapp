//
//  PokemonTableViewCell.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 18/12/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    @IBOutlet var pokemonImageView: UIImageView!
    @IBOutlet var pokemonTitleLabel: UILabel!
    @IBOutlet var pokemonTimesCaughtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
