//
//  BatttleSceneViewController.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 19/12/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit
import SpriteKit

class BatttleSceneViewController: UIViewController {

    var pokemon : Pokemon!  // tendra el pokemon enviado de ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // INSTANCIAR BattleScene inscrustada en la viewController
        let scene = BattleScene(size: CGSize(width: self.view.frame.width, height: self.view.frame.size.height))  //Tamaño de la escena usando el tamaño de la vista
        
        // INDICAR EL viewController MUESTRE LA ESCENA PERO EL viewController debe ser SKVIEW
        self.view = SKView()  // viewController la convertirmos a SKView preparada para mostar SKScene
        
        // PERSONALIZAMOS LA VISTA viewController de tipo SKVIEW
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = false  // renderizado estricto o no, False : mas rapido y menos carga en memoria NOTA: SIMPRE FALSE
        
        // MOSTRAR ESCENE
        scene.scaleMode = .aspectFill
        scene.pokemon = self.pokemon  // pasamos a la escena pokemon debe ser capturado
        skView.presentScene(scene)
        
        
        // NOTIFICACION : TRASMITE INFORMACION DENTRO DEL PROGRAMA, EL VIEWCONTROLLER ESTARA OBSERVANDO QUIEN LANZE UNA NOTIFICACION CON NOMBRE "closeBattle"
        NotificationCenter.default.addObserver(self, selector: #selector(returnToMapViewController), name: NSNotification.Name("closeBattle"), object: nil)
        
    }

    
    func returnToMapViewController(){
        self.dismiss(animated: true, completion: nil)  // Cerrar el view controller, modally presentacion
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
