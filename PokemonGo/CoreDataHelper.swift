//
//  CoreDataHelper.swift
//  PokemonGo
//
//  Created by Victor Hugo Benitez Bosques on 23/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit
import CoreData

func createAllThePokemons(){
    
    if let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer{
    
        let context = container.viewContext  // Context gestion de los objetos
        
        // Creacion de los pokemones
        createPokemon(name: "Abra", with: "abra", frequency: 70)
        createPokemon(name: "Bellsprout", with: "bellsprout", frequency: 80)
        createPokemon(name: "Bullbasaur", with: "bullbasaur", frequency: 20)
        createPokemon(name: "Caterpie", with: "caterpie", frequency: 82)
        createPokemon(name: "Charmander", with: "charmander", frequency: 20)
        createPokemon(name: "Dratini", with: "dratini", frequency: 60)
        createPokemon(name: "Eevee", with: "eevee", frequency: 50)
        createPokemon(name: "Jigglypuff", with: "jigglypuff", frequency: 67)
        createPokemon(name: "Mankey", with: "mankey", frequency: 40)
        createPokemon(name: "Meowth", with: "meowth", frequency: 1)
        createPokemon(name: "Mew", with: "mew", frequency: 2)
        createPokemon(name: "Pikachu", with: "pikachu-2", frequency: 100)
        createPokemon(name: "Psyduck", with: "psyduck", frequency: 70)
        createPokemon(name: "Rattata", with: "rattata", frequency: 17)
        createPokemon(name: "Snorlax", with: "snorlax", frequency: 3)
        createPokemon(name: "Squirtle", with: "squirtle", frequency: 10)
        createPokemon(name: "Zubat", with: "zubat", frequency: 100)
        createPokemon(name: "Weedle", with: "weedle", frequency: 60)
        
        do {
            try context.save()
        } catch let error {
            print("Erro", error.localizedDescription)
        }

    }
}


/*
//ELEMENTOS DE CUA TEST
//Usar para es tester de la aplicacion capturado un pokemon
func createAndCaughtPokemon(name: String, with nameFile: String){
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let pokemon = Pokemon(context: context) // instancia del objeto
    pokemon.name = name
    pokemon.imageName = nameFile
    pokemon.timesCaught = 1
    
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
}
*/

func createPokemon(name:String, with fileName:String, frequency : Int){
    
    // USO DE LA ENTIDAD Pokemon COMO CLASE
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext  //context gestiona los objetos y comunicacion con Core Data
    
    // Creacion del objeto con el contructor que pide solo el contxto
    let pokemon = Pokemon(context: context) //Creacion de la instancia de la entidad(Clase) Pokemon
    pokemon.name = name                     //Asignacion valores atributos del objeto
    pokemon.imageName = fileName
    pokemon.frequency = Int16(frequency)  // Agregado frecuencia de apricion
}


func getAllThePokemons() -> [Pokemon] {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext  //Necesrio para hacer la peticion a Core Data
    do{
        // FUNCION RECURSIVA SI NO ENCUENTRA POKEMONES EJECUTA LA FUNCION DE CREACION Y VUELVE A EJECUTAR EL METODO
        // RETORNANDO EL ARRAY DE POKEMONES
        
        let pokemons = try context.fetch(Pokemon.fetchRequest()) as! [Pokemon]  //peticion a Core Data de traer los objetos guardados en Core data
        if pokemons.count == 0 {
            createAllThePokemons()
            return getAllThePokemons()
        }
        return pokemons  // retornamos un array de pokemones
    }catch{
        print("Ha habido un problema al recuperar los pokemons de Core Data")
    }
    
    return []
}

func getAllCaugthPokemons() -> [Pokemon] {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext  //peticiones pasan en el contexto
    
    let fetchRequest = Pokemon.fetchRequest() as NSFetchRequest<Pokemon> //Peticion usando la entidad como clase
    fetchRequest.predicate = NSPredicate(format: "timesCaught > %d", 0) //Agregamos condicion  %@ = requiere un String
    
    do{
        let pokemons = try context.fetch(fetchRequest) as [Pokemon]  //Peticion que regresa un array de objetos de la clase Pokemon
        
        return pokemons //Regresa array de objetos Pokemon
    }catch{
        print("Ha habido un problema al recuperar los pokemons Core Data")
    }
    
    return []
}


func getAllUncoughtPokemons() -> [Pokemon] {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext  //contexto que se encarga de enlaze de comunicacion con el Core Data
   
    let fetchRequest = Pokemon.fetchRequest() as NSFetchRequest<Pokemon>  //creamos la peticion con el predicado
    fetchRequest.predicate = NSPredicate(format: "timesCaught == %d", 0) //Filtro de la peticion
    
    do{
        let pokemons = try context.fetch(fetchRequest) as [Pokemon] //Guardamos el resultado y hacemos casting ya que sabemos que obtendremos un array de Pokemones
    
        return pokemons     //Retornamos y acaba el metodo
    
    }catch{
        print("Ha habido un problema al recuperar los pokemons Core Data")
    }
    
    return []  //Regresa array vacio si  hay un error en la peticion
}









